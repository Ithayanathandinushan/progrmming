package student.lk;

import java.util.ArrayList;

public class StudentGrade {
	
	

	ArrayList<String> studentnames =new ArrayList<String>();
	private char[]Studentsgrade= {'A','B','C','S','W'};
	private double[][] StudentTestScore;
	
	public String  getstudentnames(int StudentIndex) {
		return studentnames.get(StudentIndex);
	}
		public double calculateAverageTestScore(double[]StudentTestScore) {
			double StudentTestScoreTotal=0;
			double StudentTestScoreAverage;
			
			for(int CurrentStudentTestScore=0;CurrentStudentTestScore<StudentTestScore.length;CurrentStudentTestScore++) {
				StudentTestScoreTotal=StudentTestScoreTotal+StudentTestScore[CurrentStudentTestScore];
				
			}
			StudentTestScoreAverage=StudentTestScoreTotal/StudentTestScore.length;
			return StudentTestScoreAverage;
		}
		
		public char getStudentsgrad(double StudentTestScoreAverage) {
			char Studentsgrade='z';
			
				if(StudentTestScoreAverage <45)
			    {
					Studentsgrade= 'W';
			    }
			    else if(StudentTestScoreAverage <55)
			    {
			    	Studentsgrade= 'S';
			    } 
			    else if (StudentTestScoreAverage<65)
			    {
			    	Studentsgrade= 'C';
			    }
			    else if (StudentTestScoreAverage<=75)
			    {
			    	Studentsgrade= 'B';
			    }
			    
			    else  if (StudentTestScoreAverage <100)
			        {
			    	Studentsgrade= 'A';
			        }
				
				return Studentsgrade;
				
	}
				public void setStudentName(String  StudentName) {
					studentnames.add(StudentName);
				}
				
				public double [][] setStudentTestScoreArray() {
					return StudentTestScore;
					
				}
				
				public void setStudentScore(int StudentIndex,int ScoreIndex,double StudentScore) {
					StudentTestScore[StudentIndex][ScoreIndex]= StudentScore;
				}
				
				
				public StudentGrade( int NumberOfStudent ,int NumberOfTestSorePerStudent) {
					StudentTestScore=new double[NumberOfStudent][NumberOfTestSorePerStudent];
				}
				
				public int StudentGrade(int[] StudentTestScoreAverage, int heighStudentAverage) {
					int max = heighStudentAverage;
					for (int mark : StudentTestScoreAverage) {
						if (mark > max) {
							max = mark;
						}
					}
					return max;
				}
}