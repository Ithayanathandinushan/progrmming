package student.lk;

import java.util.Scanner;

public class StudentGradeTest {
	

	public static void main(String[] args) {
		Scanner keyboard=new Scanner(System.in);
		String StudentName;
		double StudentScore;
		double [][] StudentTestScores;
		double StudentAverage;
		char Studentsgrade ;
		char max=0;
		final int NumberOfStudent=2;
		final int NumberOfTestScorePerStudent=2;
		
		StudentGrade classof2019=new StudentGrade(NumberOfStudent, NumberOfTestScorePerStudent);
		StudentTestScores=classof2019.setStudentTestScoreArray();
		
		for(int CurrentStudentIndex=0;CurrentStudentIndex<NumberOfStudent; CurrentStudentIndex++) {
			System.out.println("Enter the Student name:"+" "+(CurrentStudentIndex+1));
			StudentName=keyboard.nextLine();
		    classof2019.setStudentName(StudentName);
			
			for(int CurrentStudentScoreIndex=0;CurrentStudentScoreIndex<NumberOfTestScorePerStudent; CurrentStudentScoreIndex++) {
			System.out.println("Enter the Score of"+" "+ (CurrentStudentScoreIndex + 1)+" " + "subject"+" "+ "of"+" "+StudentName);
			StudentScore=keyboard.nextDouble();
			
			while(StudentScore<0 || StudentScore>100) {
				System.out.println("Enter the valid Score of"+ (CurrentStudentScoreIndex + 1) +" "+ " "+"of "+" "  + StudentName);
				StudentScore=keyboard.nextDouble();
			}
			
			keyboard.nextLine();
			classof2019.setStudentScore(CurrentStudentIndex,CurrentStudentScoreIndex,StudentScore);
			}
			
			
		}
		
		for(int CurrentStudentIndex=0;CurrentStudentIndex<NumberOfStudent; CurrentStudentIndex++) {
			System.out.println(classof2019.getstudentnames(CurrentStudentIndex)+" " +" "+"Score:");
			StudentAverage=classof2019.calculateAverageTestScore(StudentTestScores[CurrentStudentIndex]);
			Studentsgrade = classof2019.getStudentsgrad(StudentAverage);
			
			for(int CurrentStudentScoreIndex=0;CurrentStudentScoreIndex<NumberOfTestScorePerStudent; CurrentStudentScoreIndex++) {
				if(CurrentStudentScoreIndex!=NumberOfTestScorePerStudent-1) {
					System.out.println(StudentTestScores[CurrentStudentIndex][CurrentStudentScoreIndex]+" ");
				}
				else {
					System.out.println(StudentTestScores[CurrentStudentIndex][CurrentStudentScoreIndex]+" ");
				}
				
				
				
		}
			System.out.println("Average"+" "+String.format("%2f",StudentAverage));
			System.out.println("Grade"+" "+ Studentsgrade);
					
		
			System.out.println("highGrade"+max);
	}
	}

}
